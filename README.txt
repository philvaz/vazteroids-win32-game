Vazteroids a Win32 game by philvaz

Vazteroids is a classic 'asteroids' type 2D game written in C/C++ for Win32 using full screen GDI graphics. All resources needed to compile the game are included in this repository. It was originally compiled using Visual C++ 6.0 but can be imported and updated for modern versions of Visual Studio (e.g. 2012 and beyond).

For more see www.VazGames.com
